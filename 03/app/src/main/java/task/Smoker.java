package task;

public class Smoker implements Runnable {
    final boolean ingredient = true;
    String name;
    Bar bar;

    Smoker(Bar bar, String name) {
        this.name = name;
        this.bar = bar;
        this.bar.smokers.add(this);
    }

    @Override
    public void run() {
        bar.workSmoker();
    }
}
