package task;

public class Main {
    public static void main(String[] args) {
        Bar bar = new Bar();
        Bartender bartender = new Bartender(bar);
        Smoker client_1 = new Smoker(bar, "Астан");
        Smoker client_2 = new Smoker(bar, "Олег");
        Smoker client_3 = new Smoker(bar, "Егор");

        Thread x1 = new Thread(client_1, "Thread 1");
        Thread x2 = new Thread(client_2, "Thread 2");
        Thread x3 = new Thread(client_3, "Thread 3");
        Thread x4 = new Thread(bartender);

        x1.start();
        x2.start();
        x3.start();
        x4.start();
    }
}
