package org.example;

import java.util.ArrayList;
public class Bar {
    ArrayList<Smoker> smokers = new ArrayList<>(); // Всего курящих в баре
    ArrayList<Boolean> ingredient = new ArrayList<>(); // Ингридиенты для курения
    Smoker smokingArea; // Место для курения
    Bar() {
        smokingArea = null;
    }
/*======================================================================================================================
 * Принцип работы бармена
 */
    public synchronized void workBartender() {
        while (smokers != null) {
            if (smokingArea == null) {
                System.out.println("Бармен: Место для курения свободно, подходите пожалуйста.");
                inviteAClient();
                System.out.println("========================================================");
            }
        }
    }
    public synchronized void inviteAClient() {
        int x = (int) (Math.random() * 3);
        smokingArea = smokers.get(x);
        System.out.println("Клиент " + smokingArea.name + " сел на место для курения.");
        prepareCigarette();
    }
    public synchronized void prepareCigarette() {
        for (Smoker smoker : smokers) {
            if (smokingArea == smoker) {
                continue;
            }
            ingredient.add(smokingArea.ingredient);
        }
        System.out.println("Бармен взял ингридиенты у двух других поситителей.");
        System.out.println("Бармен скрутил сигарету и дал её клиенту " + smokingArea.name + ". ");
        notify();
        try {
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
/*======================================================================================================================
 * Принцип работы курильщика
 */
    public synchronized void workSmoker() {
        while (smokers != null) {
            if (ingredient.size() == 0) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    System.out.println("Клиент " + smokingArea.name + " курит.");
                    Thread.sleep(1000);
                    System.out.println("Клиент " + smokingArea.name + " закончил курить.");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ingredient.clear();
                smokingArea = null;
                notifyAll();
            }
        }
    }
}