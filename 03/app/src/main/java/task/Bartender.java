package task;

public class Bartender implements Runnable {
    Bar bar;
    Bartender(Bar bar) {
        this.bar = bar;
    }

    @Override
    public void run() {
        bar.workBartender();
    }
}
