package task;

import java.util.LinkedList;
import java.util.Queue;

/*
 * @author Астан Патарая
 * @version 1.1
 * Класс парекмахерская(Barbershop) описывает логику парекмахера(Hairdresser) и клиента(Client)
 */
public class Barbershop {
    /*
     * Количество мест в рчереди
     */
    final int CHAIR = 3;
    /*
     * Количество занятых стульев
     */
    int NumberOfOccupiedChairs;
    /*
     * Идёт стрижка или нет
     */
    static boolean haircut;
    /*
     * Экземпляр парикмахера
     */
    Hairdresser barber;
    /*
     * Экземпляр клиента
     */
    Client client;
    /*
     * Очередь в которой хранятся клиенты
     */
    Queue<Client> barbershopQueue = new LinkedList<>();

    /*
     * Конструктор задающий значения по умолчанию
     */
    Barbershop() {
        NumberOfOccupiedChairs = 0;
        haircut = false;
        barber = new Hairdresser(this);
    }
/*======================================================================================================================
 * Метод(workClient) описывающий принцип работы клиента
 * @param client - клиент который приходит в парекмахерскую
 */
    public synchronized void workClient(Client client) { // Описание логики клиента
        System.out.println("Клиет " + client.name + " пришёл в парикмахерскую.");
        if (haircut == false) { // Если парикмахер спит
            haircut = true;
            this.client = client;
            System.out.println("Клиет " + client.name + ": Здравствуйте, мне нужно подстричься.");
        } else {
            workClientTrue(client);
        }
        try {
            notify(); // Запускает другой поток
            wait(); // Ожидание
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

/*
 * Метод(workClientTrue) описывающий принцип работы клиента
 * @param client - клиент который приходит в парекмахерскую
 */
    public void workClientTrue(Client client) {
        if (NumberOfOccupiedChairs < CHAIR) { // Если в очереди есть место то клиент занимает в ней место
            barbershopQueue.add(client); // Клиет сел в очередь
            NumberOfOccupiedChairs++;
            System.out.println("Клиент " + client.name + ": пока парикмахер занят стрижкой, займу очередь.");
        } else {
            System.out.println("Клиент " + client.name + ": увы, но в очереди нет места, придется уйти.");
        }
    }

/*======================================================================================================================
 * Принцип работы парекмахера
 * Метод(workBarber) описывающий принцип работы парикмахера
 */
    public synchronized void workBarber() { // Описание логики парикмахера
        if (this.client == null) { // Парекмахер спит
            haircut = false;
            System.out.println(". . .");
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (this.client != null) { // Парекмахер занят стрижкой
            haircut = true;
            System.out.println("Парекмахер: Начал стрижку клиента " + this.client.name);
            try {
                wait(1000);
                System.out.println("Парекмахер: Закончил стрижку клиента " + this.client.name);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.client = null;
            if (NumberOfOccupiedChairs != 0) {
                System.out.println("Парекмахер: Позвал следующего клиента");
            }
            nextClient();
        }
    }
/*
 * Метод(nextClient) вызывает следующего клиента на стрижку
 */
    public synchronized void nextClient() {
        if (NumberOfOccupiedChairs != 0) {
            this.client = barbershopQueue.poll();
            NumberOfOccupiedChairs--;
            System.out.println("Парекмахер: Приглосил клиента " + this.client.name);
        } else {
            haircut = false;
            System.out.println(". . .");
        }
    }
}

