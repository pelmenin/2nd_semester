package task;

public class Main {
    public static void main(String[] args) {
        Barbershop s1 = new Barbershop();
        Client visitor1 = new Client(s1, "Астан");
        Client visitor2 = new Client(s1, "Олег");
        Client visitor3 = new Client(s1, "Даня");
        Client visitor4 = new Client(s1, "Баграт");
        Client visitor5 = new Client(s1, "Егор");

        Thread w1 = new Thread(visitor1, "Астан");
        Thread w2 = new Thread(visitor2, "Олег");
        Thread w3 = new Thread(visitor3, "Даня");
        Thread w4 = new Thread(visitor4, "Баграт");
        Thread w5 = new Thread(visitor5, "Егор");
        Thread tt1 = new Thread(s1.barber);

        w1.start();
        w2.start();
        w3.start();
        w4.start();
        w5.start();
        tt1.start();
    }
}
