package tasks;

public class Hairdresser implements Runnable{
    Barbershop barbershop;
    Hairdresser(Barbershop s1) {
        barbershop = s1;
    }

    @Override
    public void run() {
        while (true) {
            barbershop.workBarber();
        }
    }
}
