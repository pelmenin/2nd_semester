package org.example;

import java.util.concurrent.Semaphore;

public class Main {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(2);
        Fork fork_1 = new Fork();
        Philosopher philosopher_1 = new Philosopher(semaphore);
    }
}